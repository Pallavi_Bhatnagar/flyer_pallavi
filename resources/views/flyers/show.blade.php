@extends('layout')

@section('content')

    <div class="row">
        <div class="col-md-4">
<h1 class="projectHeading">{{ $flyer->street }}</h1>
{{--<h2>Rs {!! number_format($flyer->price) !!}</h2>--}}
<h2>{!! $flyer->price !!}</h2>

        <hr>

        <div class="description">{!! nl2br($flyer->description) !!}</div>
        </div>
        <div class="col-md-8" gallery>
            @foreach($flyer->photos->chunk(4) as $set)
                <div class="row">
                    @foreach($set as $photo)
                        <div class="col-md-3" gallery_image>
                        <img src="{{ url('/'.$photo->thumbnail_path) }}" alt="">
                        </div>
                        @endforeach
                </div>
{{--            @foreach($flyer->photos as $photo)--}}
{{--                <img src="{{ url('/'.$photo->path) }}" alt="">--}}
                {{--<img src="{{ url('/'.$photo->thumbnail_path) }}" alt="">--}}
{{--                <img src="{{ url('/flyers/photos/'.$photo->path) }}" alt="">--}}

            @endforeach

            @if($user->owns($flyer))
                <hr>
                <h2>Add your photos</h2>

                {{--<form id="addPhotosForm" action="{{ url('/').'/'.$flyer->zip.'/'.$flyer->street}}/photos" method="post" class="dropzone">--}}
                <form id="addPhotosForm" action="{{ route('store_photo_path', [$flyer->zip, $flyer->street]) }}" method="post" class="dropzone">
                    {{ csrf_field() }}
                </form>
                @endif
            <?php //dd($photo->path) ?>
        </div>
    </div>

    @stop

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
                maxFilesize: 3,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp'
        };

    </script>
{{--@include('css/style')--}}
    @stop

