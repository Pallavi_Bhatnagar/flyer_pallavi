
@extends('layout')


@section('content')


<h1 class="projectHeading">Selling your home?</h1>

<hr>

{{--<div class="row">--}}
{{--<form method="post" action="flyers" enctype="multipart/form-data" class="col-md-6">--}}
<form method="post" action="{{ url('flyers') }}" enctype="multipart/form-data">
    @include('flyers.form')

    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            {{ Session::get('message') }}
        </div>
    @endif

@if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                    @endforeach
            </ul>
        </div>
    @endif
</form>
{{--</div>--}}
    @stop