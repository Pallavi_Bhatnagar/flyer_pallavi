<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Flyer::class, function (Faker\Generator $faker) {
    return [
//        'user_id' => factory(App\User)->create()->id,
        'user_id' => 1,
//        'street' => $faker->streetAddress,
//        'city' => $faker->city,
        'street' => 'vyas colony',
        'city' => 'bikaner',
        'zip' =>334001,
        'state' => 'rajasthan',
        'country' => 'india',
        'price' => 50000,
        'description' => 'this is description'
    ];
});
