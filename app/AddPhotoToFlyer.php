<?php

namespace App;

use Illuminate\Http\UploadedFile;
use Intervention\Image;
use App\FlyerPhoto;


class AddPhotoToFlyer
{
    protected $file;
    protected $flyer;

    public function __construct(Flyer $flyer, UploadedFile $file, Thumbnail $thumbnail = null)
    {
        $this->flyer = $flyer;
        $this->file = $file;
        $this->thumbnail = $thumbnail ?: new Thumbnail;
    }

    public function save()
    {
        $photo = $this->flyer->addPhoto($this->makePhoto()); //attach the photo to the flyer

        $this->file->move($photo->baseDir(), $photo->name);// move the photo to the flyerPhotos folder

        $this->thumbnail->make($photo->path, $photo->thumbnail_path);// generate a thumbnail
    }

    public function makePhoto()
    {
        return new FlyerPhoto(['name' => $this->makeFileName()]);
    }

    /**
     * construct the name
     *
     * @return string
     */
    protected function makeFileName()
    {
        $name = sha1(
            time() . $this->file->getClientOriginalName());

        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }
}