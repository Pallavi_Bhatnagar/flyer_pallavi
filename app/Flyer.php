<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flyer extends Model
{
    protected $fillable = [
        'street',
        'city',
        'zip',
        'state',
        'country',
        'price',
        'description'
    ];


      /**
     * Find the flyer at given address.
     *
     * @param string $zip
     * @param string $street
     * @return Builder
     */
     public static function locatedAt($zip, $street)
    {
        $street = str_replace('-', ' ', $street);
        return static::where(compact('zip', 'street'))->firstOrFail();
    }

    public function getPriceAttribute($price)
    {
        return 'Rs'. number_format($price);
    }

    public function addPhoto(FlyerPhoto $photo)
    {
        return $this->photos()->save($photo);
    }
    
    public function photos()
    {
        return $this->hasMany('App\FlyerPhoto');
    }

    /**
     * A flyer is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Determine if the given user created the flyer.
     *
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }
}
