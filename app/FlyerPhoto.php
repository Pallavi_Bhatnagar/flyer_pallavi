<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class FlyerPhoto extends Model
{
    protected $table = 'flyer_photos';

    protected $fillable = [
        'path',
        'flyer_id',
        'name',
        'thumbnail_path'
    ];

    public function flyer()
    {
        return $this->belongsTo('App\Flyer');
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;

        $this->path = $this->baseDir(). '/' . $name;
        $this->thumbnail_path = $this->baseDir(). '/tn-' . $name;
    }

    public function baseDir()
    {
        return 'flyerPhotos/photos';
    }

}
