<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// comment
Route::get('/', 'pagesController@home');

Route::get('login', 'registrationController@getLogin');
Route::post('login', 'registrationController@postLogin');

Route::get('logout', 'registrationController@getLogout');

Route::get('auth/login', 'registrationController@getRegister');
//Route::post('register', function(){dd('post register');});
Route::post('register', 'registrationController@postRegister');

Route::resource('flyers', 'FlyersController');
Route::get('{zip}/{street}', 'FlyersController@show');

//Route::post('{zip}/{street}/photos', 'FlyersController@addPhotos');
//Route::post('{zip}/{street}/photos', ['as' => 'store_photo_path', 'uses' => 'FlyersController@addPhotos']);
Route::post('{zip}/{street}/photos', ['as' => 'store_photo_path', 'uses' => 'PhotosController@store']);

