<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\User;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

class registrationController extends Controller
{
    public function getRegister()
    {
        return view('auth.login');
    }

    public function postRegister(Request $request)
    {
//        $this->validate($request,[
//            'name' => 'required|max:255',
//            'email' => 'required|email|max:255|unique:users',
//            'password' => 'required|min:6|confirmed'
//        ]);

//        dd('hello');
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        Auth::attempt([
            'email' => $request['email'],
            'password' => $request['password']

        ]);
        return redirect('flyers/create');
//        return view('flyers.create');
    }

    public function getLogin()
    {
        return view('auth/login');
    }

    public function postLogin(Request $request)
    {

//        dd(auth::check());
//        dd($request->all());
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if(Auth::attempt([
        'email' => $request['email'],
        'password' => $request['password']
        ])
        ) {
            return redirect('flyers/create');
        }

        return redirect('auth/login')
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => 'These credentials do not match our records.',
            ]);

    }

    public function getLogout()
    {
        auth()->logout();

        return redirect('/');
    }
}
