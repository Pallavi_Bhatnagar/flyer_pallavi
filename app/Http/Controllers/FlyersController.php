<?php

namespace App\Http\Controllers;

use App;
use App\User;
use App\FlyerPhoto;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\FlyerRequest;

class FlyersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);

        parent::__construct();
    }

    public function create()
    {
        return view('flyers.create');
    }

    public function store(FlyerRequest $request)
    {
      $flyer = $this->user->publish(
            new App\Flyer($request->all())
        );

        \Session::flash('message', 'Flyer successfully created!');
        return redirect(flyer_path($flyer));
    }

    public function show($zip, $street)
    {

        $flyer = App\Flyer::locatedAt($zip, $street);
        return view('flyers.show', compact('flyer'));
    }
}
