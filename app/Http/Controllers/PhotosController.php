<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Flyer;
use App\FlyerPhoto;
use App\Http\Requests\AddPhotoRequest;
use App\Http\Requests;
use App\AddPhotoToFlyer;

class PhotosController extends Controller
{
    public function store($zip, $street, AddPhotoRequest $request)
    {

       $flyer = App\Flyer::locatedAt($zip, $street);
        $photo = $request->file('photo');

        (new AddPhotoToFlyer($flyer, $photo))->save();

    }
}
