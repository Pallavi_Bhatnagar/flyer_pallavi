<?php

namespace App;

use Image;
use App\FlyerPhoto;

class Thumbnail
{
    public function make($src, $destination)
    {

        Image::make($src)
            ->fit(200)
            ->save($destination);
    }
}

